package com.fms.sample.webviewresearch

import android.content.res.Configuration
import android.os.Bundle
import android.webkit.ValueCallback
import android.webkit.WebView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.webkit.WebSettingsCompat
import androidx.webkit.WebSettingsCompat.DARK_STRATEGY_PREFER_WEB_THEME_OVER_USER_AGENT_DARKENING
import androidx.webkit.WebViewCompat
import androidx.webkit.WebViewFeature
import com.fms.sample.webviewresearch.MainActivity.Companion.LOAD_HTML

class WebViewActivity : AppCompatActivity(), ValueCallback<Boolean> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        //Webview setting
        val webViewTest = findViewById<WebView>(R.id.webViewTest)
        val webSettings = webViewTest.settings
        webSettings.javaScriptEnabled = true
        webSettings.setSupportZoom(true)
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.builtInZoomControls = true
        webSettings.displayZoomControls = false

        //Start Safe Browsing
        if (WebViewFeature.isFeatureSupported(WebViewFeature.START_SAFE_BROWSING)) {
            WebViewCompat.startSafeBrowsing(this, this);
        }


        // Set Dark Mode
        val nightModeFlag = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK

        if (nightModeFlag == Configuration.UI_MODE_NIGHT_YES) {
            if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                WebSettingsCompat.setForceDark(
                    webSettings,
                    WebSettingsCompat.FORCE_DARK_ON
                )
            }

            if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK_STRATEGY)) {
                WebSettingsCompat.setForceDarkStrategy(
                    webSettings,
                    DARK_STRATEGY_PREFER_WEB_THEME_OVER_USER_AGENT_DARKENING
                )
            }
        }

        //JS Bridge
        webViewTest.addJavascriptInterface(WebAppInterface(this), "Android")

        //SET URL
        webViewTest.loadUrl(LOAD_HTML)
    }

    override fun onReceiveValue(value: Boolean?) {
        Toast.makeText(this, "Support Safe Browser : $value", Toast.LENGTH_SHORT).show()
    }
}