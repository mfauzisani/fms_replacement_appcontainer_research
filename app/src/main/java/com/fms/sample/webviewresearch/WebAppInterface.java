package com.fms.sample.webviewresearch;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * @author Fauzi Muhamad Sani (fauzi.sani@dana.id)
 * @version WebAppInterface, v 0.1 12/03/21 14.24 by Fauzi Muhamad Sani
 */
class WebAppInterface {

    Context mContext;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
}
