package com.fms.sample.webviewresearch

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.browser.customtabs.CustomTabsIntent

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnChromeCustomTabs = findViewById<AppCompatButton>(R.id.btnChromeCustomTabs)
        val btnWebView = findViewById<AppCompatButton>(R.id.btnWebView)

        btnChromeCustomTabs.setOnClickListener {
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(this, Uri.parse(URL))
        }

        btnWebView.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {

        const val URL =
            "https://link.dana.id/auth?bindDanaType=gn&acqSiteId=1022188000000000001&clientId=305XSM22SG0ASM05&state=234524252&merchantId=2020052511058602000002900939475&merchantName=adyen&netAuthId=2020061819074400000870000276910&paySiteId=1022166000000000001&redirectUrl=https://www.alipay.com&scopes=AUTH_AGREEMENTPAY,OTP_SEND&terminalType=APP&signature=ugNNhqm1RcWIqkVsUbekObFSCb6JZ24H4mzz9IAm9zcLJo0g6N0DK4PmOQ8wOjDYzYnVVJLjq2x7xD3RkMxTyqSNTCFvOTk2eQ8G/Km22L0tcSc9AVxNPWiHjVUMRV9q5Wll6sBuUXlet9+ws+zZOqKknZ/+9RVjBn5Golu2cNhwNdu5765Nk/dr4VcjsyixG5ZpXIcyFGws+K/hLKbtIh3Uw/4DFQwINzr7VW2BoEQVy1itss7iR7kCUjzWlHDDcOc0asu8i3/bJux0FsGktieVSd3y6xQhBlOIEEHMKPrBENPG18I6ffTIqLfqbny9IM3FlKzlQWDd8E2rbNYO8A=="

        const val URL_GOOGLE = "https://www.google.co.id"

        const val URL2 = "https://m.dana.id/m/ipg/inputphone?phoneNumber=&ipgForwardUrl=%2Fm%2Fportal%2Foauth%3FbindDanaType%3Dgn%26acqSiteId%3D1022188000000000001%26clientId%3D305XSM22SG0ASM05%26state%3D234524252%26merchantId%3D2020052511058602000002900939475%26merchantName%3Dadyen%26netAuthId%3D2020061819074400000870000276910%26paySiteId%3D1022166000000000001%26redirectUrl%3Dhttps%253A%252F%252Fwww.alipay.com%26scopes%3DAUTH_AGREEMENTPAY,OTP_SEND%26terminalType%3DAPP%26signature%3DugNNhqm1RcWIqkVsUbekObFSCb6JZ24H4mzz9IAm9zcLJo0g6N0DK4PmOQ8wOjDYzYnVVJLjq2x7xD3RkMxTyqSNTCFvOTk2eQ8G%252FKm22L0tcSc9AVxNPWiHjVUMRV9q5Wll6sBuUXlet9%2520ws%2520zZOqKknZ%252F%25209RVjBn5Golu2cNhwNdu5765Nk%252Fdr4VcjsyixG5ZpXIcyFGws%2520K%252FhLKbtIh3Uw%252F4DFQwINzr7VW2BoEQVy1itss7iR7kCUjzWlHDDcOc0asu8i3%252FbJux0FsGktieVSd3y6xQhBlOIEEHMKPrBENPG18I6ffTIqLfqbny9IM3FlKzlQWDd8E2rbNYO8A%253D%253D%26_branch_match_id%3D775630963948496090%26_hasLogout%3Dtrue&clientId=305XSM22SG0ASM05&merchantId=2020052511058602000002900939475"

        const val LOAD_HTML ="file:///android_asset/index.html"
    }
}